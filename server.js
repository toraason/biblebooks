// server.js
// load the things we need
var express = require('express');
var jsonfile = require('jsonfile');
var url = require('url');
var app = express();

// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(express.static('static'))

// use res.render to load up an ejs view file

// Read the book data from a file when server first starts
// Someday might be nice to allow for alternate lists
var bookfilename = 'data/booklist.json';
var allBooks = [];   // This gets populated asynchronously as soon as the file is read
jsonfile.readFile(bookfilename,function(err,resultObj) {
  if (resultObj!=undefined) {
    allBooks = resultObj;
  } else {
    console.log(`Unable to read ${bookfilename} and parse JSON`);
    console.log(err);
  }
});

function renderList(req,res,template,username) {
  if (username!=undefined) {
    var userfilename = 'data/'+username+'_readlist.json';
    var userReadList = {};
    jsonfile.readFile(userfilename, function(err,obj) {
      if (obj!=undefined) {
        userReadList = obj;
        res.render(template, {
          books: allBooks,
          readList: userReadList
        });
      } else {
        console.log('Could not read file and parse JSON');
        console.log(err);
        res.render(template, {
          books: allBooks
        });
      }
    });
  } else {
      res.render(template, {
        books: allBooks       
      });
  }
}



// index page 
app.get('/', function(req, res) {
    var queryData = url. parse(req.url, true).query;
    renderList(req,res,'index.ejs',queryData.user);
});


app.get('/alt1', function(req, res) {
    var queryData = url. parse(req.url, true).query;
    renderList(req,res,'alt1.ejs',queryData.user);
});


app.get('/alt2', function(req, res) {
    var queryData = url. parse(req.url, true).query;
    renderList(req,res,'alt2.ejs',queryData.user);
});

app.get('/alt3', function(req, res) {
    var queryData = url. parse(req.url, true).query;
    renderList(req,res,'alt3.ejs',queryData.user);
});

app.get('/alt4', function(req, res) {
    var queryData = url. parse(req.url, true).query;
    renderList(req,res,'alt4.ejs',queryData.user);
});

app.get('/alt5', function(req, res) {
    var queryData = url. parse(req.url, true).query;
    renderList(req,res,'alt5.ejs',queryData.user);
});

app.get('/alt6', function(req, res) {
    var queryData = url. parse(req.url, true).query;
    renderList(req,res,'alt6.ejs',queryData.user);
});

app.get('/alt7', function(req, res) {
    var queryData = url. parse(req.url, true).query;
    renderList(req,res,'alt7.ejs',queryData.user);
});

app.get('/bookshelf', function(req, res) {
    res.render('bookshelf.ejs', {
       books: allBooks
    });
});


app.get('/styles.css', function(req, res) {
    res.render('styles.ejs');
});

app.listen(8080);
console.log('8080 is the magic port');
