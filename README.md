# README #

A simple Node.js server app that displays graphically all of the books of the Bible and optionally
displays progress on reading from a user-specific JSON file.

### How do I get set up? ###

* Clone {this-repository}: `$ git clone {this-repo}`
* `$ cd biblebooks`
* `$ npm start`
* [http://localhost:8080/](http://localhost:8080/)
* [http://localhost:8080/?user=test](http://localhost:8080/?user=test)
* Edit or create `data/{user}-readlist.json` files to record reading progress.

### Who do I talk to? ###

* Owner: Dan Toraason `mailto: dan@toraason.com`
* Graphic icons from: [Overviewbible.com](https://overviewbible.com/free-bible-icons/)